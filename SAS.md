SAS es un lenguaje de programación desarrollado por SAS Institute a 
finales de los años sesenta. 
Existen dos intérpretes de dicho lenguaje: uno desarrollado por SAS 
Institute y otro por la empresa World Programming. 
SAS Institute comercializa paquetes de procedimientos adicionales para 
el análisis estadístico de los dato.
